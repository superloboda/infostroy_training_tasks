package com.examle;

public class EmailRecord {

	private String userId;
	private String userName;
	private String greeting;
	private String orderTotal;

	private EmailRecord(Builder builder) {
		userId = builder.userId;
		userName = builder.userName;
		greeting = builder.greeting;
		orderTotal = builder.orderTotal;
	}

	private EmailRecord() {
	}

	public static Builder newBuilder() {
		return new EmailRecord().new Builder();
	}

	public class Builder {

		private String userId;
		private String userName;
		private String greeting;
		private String orderTotal;

		public Builder(){
			
		}
		
		public Builder userId(String userId) {
			this.userId = userId;
			return this;
		}

		public Builder userName(String userName) {
			this.userName = userName;
			return this;
		}

		public Builder greeting(String greeting) {
			this.greeting = greeting;
			return this;
		}

		public Builder orderTotal(String orderTotal) {
			this.orderTotal = orderTotal;
			return this;
		}

		public EmailRecord build() {
			return new EmailRecord(this);
		}
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public String getGreeting() {
		return greeting;
	}

	public String getOrderTotal() {
		return orderTotal;
	}

}
