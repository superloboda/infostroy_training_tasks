package com.example.entity;

import com.example.annotation.Column;
import com.example.annotation.Table;

@Table(name = "PERSONS")
public class Person {

	@Column(name = "person_id")
	private Integer personId;

	private String name;

	private String email;

	private Integer age;

	@Column(name = "phone_number")
	private String phoneNumber;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
