package com.example;

import java.lang.reflect.InvocationTargetException;

import com.example.entity.Dog;
import com.example.entity.Person;
import com.example.handler.EntityHandler;

public class Demo {
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		EntityHandler handler = new EntityHandler();
		handler.createTable(Dog.class);		
		handler.createTable(Person.class);
	}
}
