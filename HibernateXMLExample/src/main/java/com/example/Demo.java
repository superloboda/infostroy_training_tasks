package com.example;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.example.entity.Person;
import com.example.persistence.EventManager;
import com.example.persistence.HibernateUtil;

public class Demo {
	public static final Logger logger = LogManager.getLogger(Demo.class);

	public static void main(String[] args) {
		EventManager manager = new EventManager();
		HibernateUtil.getSessionFactory().openSession();
		
		HibernateUtil.getSessionFactory().close();
	}
}
