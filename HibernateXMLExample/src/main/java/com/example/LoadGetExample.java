package com.example;

import org.hibernate.Session;

import com.example.entity.Person;
import com.example.persistence.HibernateUtil;

public class LoadGetExample {

	public static void main(String[] args) {
		scenario1();
	}

	private static void scenario1() {
		// SCENARIO 1 The record is in the session cache when load() is invoked.
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		System.out.println("Person load...");
		Person personLoad = (Person) session.load(Person.class, 6);

		System.out.println("Person get...");
		Person personGet = (Person) session.get(Person.class, 6);
		System.out.println(personGet.toString());

		
		HibernateUtil.getSessionFactory().close();
	}

	private static void scenario2() {
		// SCENARIO 2 The record is not in the session cache when load() is
		// invoked.
		Session session = HibernateUtil.getSessionFactory().openSession();

		System.out.println("Person get...");
		Person personGet = (Person) session.get(Person.class, 6);
		System.out.println(personGet.toString());

		System.out.println("Person load...");
		Person personLoad = (Person) session.load(Person.class, 7);
		System.out.println(personLoad.toString());

		HibernateUtil.getSessionFactory().close();
	}
	
	private static void scenario3() {
		// SCENARIO 3 The record is not in the session cache when load() is
		// invoked.
		Session session = HibernateUtil.getSessionFactory().openSession();

		System.out.println("Person get...");
		Person personGet = (Person) session.get(Person.class, 6);
		System.out.println(personGet.toString());

		System.out.println("Person load...");
		Person personLoad = (Person) session.load(Person.class, 5);
		System.out.println(personLoad.toString());

		HibernateUtil.getSessionFactory().close();
	}
}
