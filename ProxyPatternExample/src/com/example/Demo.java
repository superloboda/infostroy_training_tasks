package com.example;

public class Demo {
	
	public static void main(String[] args) {
		BombDefuserRobotProxy connector = new BombDefuserRobotProxy(35);
		connector.moveForward(10);
		connector.turnRight();
		connector.moveForward(3);
		connector.defuseBomb();		
	}
}
